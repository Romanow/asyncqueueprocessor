package ru.romanow.queue.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.romanow.queue.processor.items.ProcessingItem;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Created by ronin on 26.04.16
 */
public final class QueueManager
        implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(QueueManager.class);

    private volatile boolean stop;
    private final Thread thread;
    private final QueueClient<? extends ProcessingItem> queueClient;

    public QueueManager(QueueClient<? extends ProcessingItem> queueClient) {
        this.queueClient = queueClient;
        this.thread = new Thread(this, "QueueManager");
    }

    @PostConstruct
    public void start() {
        logger.info("Starting queue client");
        thread.start();
    }

    @PreDestroy
    public void stop() {
        logger.info("Stopping queue client");
        stop = true;
    }

    @Override
    public void run() {
        while (!stop) {
            try {
                queueClient.processQueueItem();
            } catch (InterruptedException exception) {
                stop = true;
            }
        }
    }
}
