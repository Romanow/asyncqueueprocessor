package ru.romanow.queue.client;

import ru.romanow.queue.processor.items.ProcessingItem;

/**
 * Created by ronin on 25.04.16
 */
public interface QueueClient<T extends ProcessingItem> {
    void processQueueItem() throws InterruptedException;
}
