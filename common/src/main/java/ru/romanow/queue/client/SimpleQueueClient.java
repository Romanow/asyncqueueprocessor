package ru.romanow.queue.client;

import org.redisson.RedissonClient;
import org.redisson.core.RBlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.romanow.queue.processor.ItemProcessor;
import ru.romanow.queue.processor.exceptions.ProcessingException;
import ru.romanow.queue.processor.items.ProcessingItem;

import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by ronin on 25.04.16
 */
public class SimpleQueueClient<T extends ProcessingItem>
        implements QueueClient<T> {
    protected static final Logger logger = LoggerFactory.getLogger(QueueClient.class);

    protected final RedissonClient client;
    protected final ItemProcessor<T> processor;
    protected final ThreadPoolExecutor executor;
    protected final RBlockingQueue<T> globalQueue;

    public SimpleQueueClient(RedissonClient redissonClient,
                             ThreadPoolExecutor threadPoolExecutor,
                             ItemProcessor<T> processor, String queueName) {
        this.client = redissonClient;
        this.executor = threadPoolExecutor;
        this.processor = processor;

        this.globalQueue = redissonClient.getBlockingQueue(queueName);
    }

    @Override
    public void processQueueItem()
            throws InterruptedException {

        processItem(globalQueue.take());
    }

    protected void processItem(T item) {
        try {
            if (item != null) {
                executor.execute(() -> processor.process(item));
            }
        } catch (ProcessingException exception) {
            logger.error("Processing exception [{}] during process item {}", exception.getMessage(), item.getItemId());
            processFailedItem(item);
        } catch (RejectedExecutionException exception) {
            logger.error("Item {} rejected: {}", item.getItemId(), exception.getMessage());
            processRejectedItem(item);
        } catch (Exception exception) {
            logger.error("Exception during process item " + item.getItemId(), exception);
        }
    }

    protected void processFailedItem(T item) {
        // TODO add to failed queue
    }

    protected void processRejectedItem(T item) {
        globalQueue.add(item);
    }
}
