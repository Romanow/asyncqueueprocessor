package ru.romanow.queue.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.romanow.queue.processor.items.ProcessingItem;

/**
 * Created by ronin on 25.04.16
 */
public class SimpleItemProcessor<T extends ProcessingItem>
        implements ItemProcessor<T> {
    protected static final Logger logger = LoggerFactory.getLogger(SimpleItemProcessor.class);

    @Override
    public void process(T item) {
        logger.info("Processed item {}", item);
    }
}
