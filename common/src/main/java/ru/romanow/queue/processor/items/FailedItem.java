package ru.romanow.queue.processor.items;

/**
 * Created by ronin on 25.04.16
 */
public interface FailedItem {
    int getRetryCount();

    int incrementRetryCount();
}
