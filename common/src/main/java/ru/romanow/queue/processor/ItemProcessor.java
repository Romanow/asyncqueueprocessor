package ru.romanow.queue.processor;

import ru.romanow.queue.processor.items.ProcessingItem;

/**
 * Created by ronin on 25.04.16
 */
public interface ItemProcessor<T extends ProcessingItem> {
    void process(T item);
}
