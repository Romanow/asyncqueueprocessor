package ru.romanow.queue.processor.items;

/**
 * Created by ronin on 25.04.16
 */
public class FailedEmptyItem
        extends EmptyItem
        implements ProcessingItem,
                   FailedItem {
    private static final long serialVersionUID = -6660522606129725455L;

    private int retryCount;

    public FailedEmptyItem() {
        this.retryCount = 0;
    }

    @Override
    public int getRetryCount() {
        return retryCount;
    }

    @Override
    public int incrementRetryCount() {
        return ++retryCount;
    }
}
