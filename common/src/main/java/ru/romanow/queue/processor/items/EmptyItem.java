package ru.romanow.queue.processor.items;

import com.google.common.base.MoreObjects;

import java.util.UUID;

/**
 * Created by ronin on 25.04.16
 */
public class EmptyItem
        implements ProcessingItem {
    private static final long serialVersionUID = -8534982718118695535L;

    private UUID itemId;

    public EmptyItem() {}

    public EmptyItem(UUID itemId) {
        this.itemId = itemId;
    }

    @Override
    public UUID getItemId() {
        return itemId;
    }

    public void setItemId(UUID itemId) {
        this.itemId = itemId;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                          .add("itemId", itemId)
                          .toString();
    }
}
