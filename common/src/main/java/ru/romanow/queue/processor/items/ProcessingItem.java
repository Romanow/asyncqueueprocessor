package ru.romanow.queue.processor.items;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by ronin on 25.04.16
 */
public interface ProcessingItem
        extends Serializable {

    UUID getItemId();
}
