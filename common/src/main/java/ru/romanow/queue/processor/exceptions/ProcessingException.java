package ru.romanow.queue.processor.exceptions;

/**
 * Created by ronin on 25.04.16
 */
public class ProcessingException
        extends RuntimeException {
    private static final long serialVersionUID = -2469266718176544471L;

    public ProcessingException() {}

    public ProcessingException(String message) {
        super(message);
    }

    public ProcessingException(String message, Throwable cause) {
        super(message, cause);
    }
}
