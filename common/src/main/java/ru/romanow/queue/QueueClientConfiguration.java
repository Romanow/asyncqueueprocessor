package ru.romanow.queue;

import org.redisson.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.concurrent.ThreadPoolExecutorFactoryBean;
import ru.romanow.queue.client.QueueClient;
import ru.romanow.queue.client.QueueManager;
import ru.romanow.queue.client.SimpleQueueClient;
import ru.romanow.queue.processor.SimpleItemProcessor;
import ru.romanow.queue.processor.items.EmptyItem;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by ronin on 25.04.16
 */
@Configuration
@Import(RedisConfiguration.class)
public class QueueClientConfiguration {

    @Autowired
    private RedissonClient redissonClient;

    @Bean
    public ThreadPoolExecutorFactoryBean threadPoolExecutor(@Value("${node.id:Worker}") String nodeId) {
        ThreadPoolExecutorFactoryBean executorFactoryBean = new ThreadPoolExecutorFactoryBean();
        executorFactoryBean.setQueueCapacity(100);
        executorFactoryBean.setThreadNamePrefix(nodeId);
        return executorFactoryBean;
    }

    @Bean
    @Autowired
    public QueueClient<EmptyItem> emptyItemQueueClient(ThreadPoolExecutor threadPoolExecutor) {
        return new SimpleQueueClient<>(redissonClient, threadPoolExecutor, new SimpleItemProcessor<>(), "emptyQueue");
    }

    @Bean
    public QueueManager queueManager(@Qualifier("emptyItemQueueClient") QueueClient<EmptyItem> queueClient) {
        return new QueueManager(queueClient);
    }
}
