package ru.romanow.queue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

/**
 * Created by ronin on 25.04.16
 */
@Profile({ "dev", "prod" })
@SpringBootApplication(scanBasePackages = "ru.romanow.queue")
@Import({ RedisConfiguration.class, QueueClientConfiguration.class })
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
