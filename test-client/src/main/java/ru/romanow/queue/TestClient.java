package ru.romanow.queue;

import org.redisson.RedissonClient;
import org.redisson.core.RQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.romanow.queue.processor.items.EmptyItem;

import java.util.UUID;

/**
 * Created by ronin on 25.04.16
 */
@Service
public class TestClient {
    private static final Logger logger = LoggerFactory.getLogger(TestClient.class);
    private static final String QUEUE_NAME = "emptyQueue";

    @Autowired
    private RedissonClient redissonClient;

    public void generate(int count) {
        RQueue<EmptyItem> queue = redissonClient.getQueue(QUEUE_NAME);
        for (int i = 0; i < count; i++) {
            EmptyItem item = new EmptyItem(UUID.randomUUID());
            logger.info("Create item [{}] and push it to queue {}", item.getItemId(), QUEUE_NAME);
            queue.add(item);
        }
    }
}
