package ru.romanow.queue;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

/**
 * Created by ronin on 25.04.16
 */
@Profile("dev")
@Configuration
@EnableAutoConfiguration
@ComponentScan(value = "ru.romanow.queue", excludeFilters = @ComponentScan.Filter(Configuration.class))
@Import(RedisConfiguration.class)
public class TestApplication
        implements InitializingBean {

    private static final int COUNT = 50;

    @Autowired
    private TestClient testClient;

    @Autowired
    private ReactiveClient reactiveClient;

    public static void main(String[] args) {
        SpringApplication.run(TestApplication.class, args);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
//        testClient.generate(COUNT);
        reactiveClient.test();
    }
}
