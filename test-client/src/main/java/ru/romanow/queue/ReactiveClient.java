package ru.romanow.queue;

import org.redisson.RedissonClient;
import org.redisson.core.RBlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.romanow.queue.processor.items.EmptyItem;

import javax.annotation.PostConstruct;

/**
 * Created by ronin on 26.04.16
 */
@Service
public class ReactiveClient {
    private static final Logger logger = LoggerFactory.getLogger(ReactiveClient.class);
    private static final String QUEUE_NAME = "emptyQueue";

    @Autowired
    private RedissonClient client;
    private RBlockingQueue<EmptyItem> queue;

    @PostConstruct
    public void init() {
        queue = client.getBlockingQueue(QUEUE_NAME);
    }

    public void test() {


        /*
        Flux.create((s) -> s.onNext(getItem()))
            .doOnError((ex) -> logger.error("", ex))
            .consume((item) -> logger.info("{}", item));
            */
    }

    private EmptyItem getItem() {
        try {
            return queue.take();
        } catch (InterruptedException exception) {
            throw new RuntimeException(exception);
        }
    }
}
